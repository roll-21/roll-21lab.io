# Roll 21

## Licenses

[Dungeon World](https://dungeon-world.com/) by [Sage LaTorra](http://www.latorra.org/) and [Adam Koebel](https://www.adam-koebel.com/) is licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)
